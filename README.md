1. Clone the reposirity to you end.

2. run "npm install"

3. Following are the end points
    
        1. localhost:3000/users/register          method = POST
            input=> {email="",username="",password=""}
            
        2. localhost:3000/users/login             method = POST
            input=> {email="",password=""}
            
        3. localhost:3000/users/email-verification/:userid=""         method = GET
        
        4. localhost:3000/users/getdetails             method= POST
            input=> {email=""}
            
        5. localhost:3000/users/forgotpassword                 method= POST
            input=> {email=""}
            
        6. localhost:3000/users/changepassword             method = POST
            input=> {email="",uniqueNumber="",newPassword="",confirmPassword=""}
            
        7. localhost:3000/users/verifytoken                 method=  POST
            input = {email=""}
            
        8. localhost:3000/users/logout method = POST
            input => {email=""}
    
4. You can change the email, password and mongodb url in config.js file to check those api and functionality.