const jwt = require('jsonwebtoken');
const randomize = require('randomatic');
const uuid = require('uuid/v4');
const User = require('../models/user');
const mailer = require('../utility/mailer');
const validator = require('../utility/validator');



exports.register = (req,res) => {

    let uniqueID = null;
    const {username, email, password} = req.body;

    let newUser = null;

    if(!validator.validateEmail(email)){

       return res.json({
            status : 'Error!!!!!',
            message: 'Please enter the correct email in correct format'
        })

    }


    // create a new user if does not exist
    const create = (user) => {
        if(user) {
            throw new Error('Email Already Exist!!!')
        } else {
            uniqueID = uuid();
            return User.create(username, email, password , uniqueID)
        }
    };

    const sendmail=(user) => {

        console.log(uniqueID);
        mailer.sendMail(user.username,user.email,uniqueID);
    };

    // respond to the client
    const respond = () => {
        console.log('this is respond');
        res.json({
            status: 'Registered Successfully',
            message:'Check your mail for verification'
        });
    };

    // run when there is an error (username exists)
    const onError = (error) => {
        res.status(409).json({
            message: error.message
        })
    };

    // check username duplication
    User.findOneByUserEmail(email)
        .then(create)
        .then(sendmail)
        .then(respond)
        .catch(onError)
};

exports.verfiyUUID = (req,res) => {

    const uniqueID  = req.params.userid;

    console.log(uniqueID);

    const check = (user) =>{
        if(!user){
            throw new Error('User Does Not Found');
        }else{
                if(user.verifyUniqueID(uniqueID)){
                    return user;
                }else{

                    throw new Error('Check your mail, verify yourself then try login');
                }

        }
    };

    const updateStatus = (user) => {
        console.log("this is update Status");
        console.log(user);

        if(User.updateStatus(uniqueID)){

            return (user)

        }else{
            throw new Error('User Id Mismatch Try Again!!!');
        }

    };

    const respond = (user) =>{
        res.json({
            Status : 'Success',
            Message : 'Account successfully verified. You can login now.',
            Email :user.email,

        })
    };

    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };

    User.findOneByUniqueID(uniqueID)
        .then(check)
        .then(updateStatus)
        .then(respond)
        .catch(onError)
};



exports.login = (req, res) => {

    const {email, password} = req.body;
    console.log(password)
    const secret = req.app.get('jwt-secret');

    // check the user info & generate the jwt


    const check = (user) => {

        if(!user) {
            // user does not exist
            throw new Error('Not a valid Email ')
        } else {
            if(!user.isActive(user)){
                throw new Error('Email Not verified')
            }else{
                console.log(user);
                if(user.verifyPassword(password)) {
                    console.log('Nimesh Token'+ (user.jwtToken));
                    console.log(user);
                    //accessToken = user.accessToken;
                    console.log('Nimesh Token'+ (user.jwtToken));

                    if(user.jwtToken === null){
                        console.log('I am here');
                        const p = new Promise((resolve, reject) => {
                            jwt.sign(
                                {
                                    _id: user._id,
                                    username: user.email,
                                    status: user.active,
                                    ip : user.ip
                                },
                                secret,
                                {
                                    expiresIn: '10days',
                                    issuer: 'nimesh shah',
                                    subject: 'authentication',

                                }, (err, token) => {
                                    if (err) reject(err);
                                    User.setJWTToken(user.email, token);
                                    resolve(token)
                                });
                        });
                        return p;

                    }else{


                    }
                             

                } else {
                    throw new Error('Not a Valid Password')
                }
            }
            // user exists, check the password

        }
    };

    // respond the token
    const respond = (token) => {

        res.json({
            message: 'logged in successfully',
            token,

        })
    };

    // error occured
    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };

    // find the user
    User.findOneByUserEmail(email)
        .then(check)
        .then(respond)
        .catch(onError)

};


exports.userDetails = (req,res) =>{

    let email = req.body.email;
    
    const checkUser = (user) => {

        if (!user)
            throw new Error();

        return user;
    }
    
    const respond = (user) =>{
        res.json({
            Status : 'Success',
            Message : {username:user.username,
                        email:user.email,
                        status:user.active},
            });
    };

    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };

    User.findOneByUserEmail(email)
        .then(checkUser)
        .then(respond)
        .catch(onError)


}


exports.forgotPassword = (req,res) => {
    
    const { email} = req.body;

    if(!validator.validateEmail(email)){

       return res.json({
            status : 'Error!!!!!',
            message: 'Please enter the correct email in correct format'
        })

    }

  let uniqueNumber=null;
    // create a new user if does not exist
    const create = (user) => {
        
            uniqueNumber = randomize('0', 6);
            console.log("This is unique number"+uniqueNumber);
        return User.updateForgotPasswordUniqueNumber( email, uniqueNumber);
        
    };

    const sendmail=(user) => {

        console.log(uniqueNumber);
        mailer.sendMailForgotPassword(user.username,user.email,uniqueNumber);
    };

     // respond to the client
    const respond = () => {
        console.log('this is respond');
        res.json({
            status: 'sucess',
            message:'Check your mail to change the password.'
        });
    };

    // run when there is an error (username exists)
    const onError = (error) => {
        res.status(409).json({
            message: error.message
        })
    };

    // check username duplication
    User.findOneByUserEmail(email)
        .then(create)
        .then(sendmail)
        // .then(count)
        .then(respond)
        .catch(onError)
};


exports.changePassword = (req,res) => {

    const {email, uniqueNumber, newPassword, confirmPassword} = req.body;

    const check = (user) =>{
        if(newPassword != confirmPassword){
            throw new Error('Password does not match.');
        }else{
                if(user){
                    return user;
                }else{

                    throw new Error('Check your email, email does not found');
                }

        }
    };

    const verifyUniqueNumber = (user) =>{

        console.log(user)
        if(!user.verifyUniqueNumber(uniqueNumber))
            throw new Error("Unique numbe rdoes not match, please check your mail and try again.");
        else
            return user;
    }

    const updatePassword = (user) => {
        console.log("this is update password number");
        console.log(user);

        if(User.setPassword(email,newPassword)){
            return (user)

        }else{
            throw new Error("Unique number does not match, please check and try again.");
        }

    };


    const respond = (user) =>{
        res.json({
            Status : 'Success',
            Message : 'Password changed sucessfully. You can login now.',
            Email :user.email,

        })
    };

    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };

    User.findOneByUserEmail(email)
        .then(check)
        .then(verifyUniqueNumber)
        .then(updatePassword)
        .then(respond)
        .catch(onError)
};

exports.getJWTToken = (req,res) => {

    let email = req.body.email;

    const getJWTToken = (user) =>{

        if(!user)
            throw new Error('User Not Found');

        return user;
    }

    const respond = (user) => {
        res.json({
            email: user.email,
            jwtToken : user.jwtToken
        });
    };

    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };


    User.findOneByUserEmail(email)
        .then(getJWTToken)
        .then(respond)
        .catch(onError)


}

exports.signout = (req,res) => {

    const {email} = req.body;

    const check = (user) =>{
        if(!User.findOneByUserEmail(email)){
            throw new Error('User not found.');
        }else{
                if(user){
                    return user;
                }else{

                    throw new Error('Check your email, email does not found');
                }

        }
    };

    const updateLoggedStatus = (user) => {
        console.log("this is update password number");
        console.log(user);

        if(User.updateLogedStatus(email)){
            return (user)

        }else{
            throw new Error("Email Id Mismatch Try Again!!!");
        }

    };


    const respond = (user) =>{
        res.json({
            Status : 'Success',
            Message : 'Logged out sucessfully.',
            Email :user.email,

        })
    };

    const onError = (error) => {
        console.log("this is error");
        res.status(403).json({
            message: error.message
        })
    };

    User.findOneByUserEmail(email)
        .then(check)
        .then(updateLoggedStatus)
        .then(respond)
        .catch(onError)
};
