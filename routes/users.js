var express = require('express');
var router = express.Router();

const user = require('../controllers/user.controller');



/**
 * Register the user
 *
 * INPUT :-  username, email, password
 *
 */
router.post('/register', user.register);

/**
 * Login User
 *
 * INPUT :- email, password
 *
 */

router.post('/login',user.login);


/**
 * Verify User SignIN
 *
 *
 */
router.get('/email-verification/:userid',user.verfiyUUID);

/**
 *
 * Get the User Details
 *
 * INPUT : email
 */
router.post('/getdetails',user.userDetails);

/**
 *
 * Get the JWT token
 *
 * INPUT : email
 */
router.post('/verifytoken',user.getJWTToken);

/**
 *
 * Set the forgot Password 
 *
 * INPUT : email
 *
 */
router.post('/forgotpassword',user.forgotPassword);


/**
 *
 * change the passsword
 *
 * INPUT : email, uniqueNumber, newPassword, confirmPassword
 *
 */
router.post('/changepassword',user.changePassword);

/**
 *
 * Sign Out
 *
 * INPUT : email
 *
 */
router.post('/signout',user.signout);

module.exports = router;
