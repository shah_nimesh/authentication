const nodemailer = require('nodemailer');
const config = require('../config');



exports.sendMail = (userName,userEmail, uniqueID) =>{

    const URL = 'http://'+config.domainName+'/users/email-verification/';

    console.log("Sender Email id:"+config.emailSender);
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true, // true for 465, false for other ports
            
            auth: {
                user: config.emailSender, // generated ethereal user
                pass: config.emailSenderPassword // generated ethereal password
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: "Nimesh Shah" , // sender address
            to: userEmail, // list of receivers
            subject: 'Please confirm your email', // Subject line
            html: '<p style="font-size: medium">Hi,&nbsp;'+userName+'<br><br>Please click the confirmation link below to confirm your account:<br><br> '
                      +URL+uniqueID+'<br><br>Best Regards,<br> Nimesh Shah</p>',

        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log(info);

        });
}

exports.sendMailForgotPassword = (userName,userEmail, uniqueNumber) =>{

    console.log("Sender Email id:"+config.emailSender);
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true, // true for 465, false for other ports
            
            auth: {
                user: config.emailSender, // generated ethereal user
                pass: config.emailSenderPassword // generated ethereal password
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: "Nimesh Shah" , // sender address
            to: userEmail, // list of receivers
            subject: 'Please enter this 6 digit unique number to change your passsowrd', // Subject line
            html: '<p style="font-size: medium">Hi,&nbsp;'+userName+'<br><br>Please enter the below 6 digit unique number to change your passsowrd.<br><br><b> '
                      +uniqueNumber+'</b><br><br>Best Regards,<br> Nimesh Shah</p>',

        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log(info);

        });
}


