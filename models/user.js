    const mongoose =require('mongoose');
    const Schema = mongoose.Schema;
    const crypto = require('crypto');
    const config = require('../config');
  

     const User = new Schema({
        username: String,
        email: String,
        password: String,
        active: {type: Boolean, default:false},
        logged:{type:Boolean, default:false},
        jwtToken:{type:String, default:null},
        uuid : {type: String},
        uniqueForgotPasswordNumber:{type:String}
    });

    // create new User document
    User.statics.create = function(username, email, password ,uniqueID) {

        const encrypted = crypto.createHmac('sha1', config.secret)
            .update(password)
            .digest('base64');

        const user = new this({
            username : username,
            email : email,
            password: encrypted,
            uuid : uniqueID

        });

        //console.log('created user sucessfully');
        // return the Promise
        return user.save()
    };

    // find one user by using email
    User.statics.findOneByUserEmail = function(email) {
        return this.findOne({ email: email}).exec()
    };

    //find the user by using uniqueID
    User.statics.findOneByUniqueID = function(uniqueID){
        return this.findOne({uuid:uniqueID}).exec();
    };

    // Verify the uuid of the User document
    User.methods.verifyUniqueID = function(uniqueID){
        return this.uuid === uniqueID;
    };

    //Verify the unique number
    User.methods.verifyUniqueNumber = function(uniqueNumber){
        console.log(uniqueNumber)
        return this.uniqueForgotPasswordNumber === uniqueNumber;
    };

    //To check if usesr has verified email or not
    User.methods.isActive = function(user){
        return user.active;
    };

    //Verifies the email of the User Document
    User.statics.updateStatus = function(uniqueId){

        console.log("this is user model");
        let query = {uuid:uniqueId};
        let update ={active: true};
        return  this.findOneAndUpdate(query,update).exec();
    };

    //To change the status of logged in
    User.statics.updateLogedStatus = function(email){

        console.log("this is user model");
        let query = {email:email};
        let update ={logged: false};
        return  this.findOneAndUpdate(query,update).exec();
    };

    //To update the forgot password unique number
    User.statics.updateForgotPasswordUniqueNumber = function(email,uniqueNumber){

        console.log("this is user model");
        let query = {email:email};
        let update ={uniqueForgotPasswordNumber: uniqueNumber};
        return  this.findOneAndUpdate(query,update).exec();
    };

    //Sets the user accessToken of the User Document
    User.statics.setAccessToken = function(email,accessToken){
        let query = {email:email};
        let update ={accessToken: accessToken};
        return  this.findOneAndUpdate(query,update).exec();

    };

    //To change the password
    User.statics.setPassword= function(email,newPassword){
        console.log(email+ newPassword)
        const encryptedPassword = crypto.createHmac('sha1', config.secret)
            .update(newPassword)
            .digest('base64');
        let query = {email:email};
        let update ={password: encryptedPassword};
        return  this.findOneAndUpdate(query,update).exec();

    };

   
    // To set the JWT token
    User.statics.setJWTToken = function(email,jwtToken){

        console.log(' JWT Token'+jwtToken)

        let query ={email : email};
        let update = {jwtToken : jwtToken, logged:true};
        return this.findOneAndUpdate(query,update).exec();

    }

    // verify the password of the User documment
    User.methods.verifyPassword = function(password) {
        const encrypted = crypto.createHmac('sha1', config.secret)
            .update(password)
            .digest('base64');
        console.log(this.password === encrypted);

        return this.password === encrypted
    };

    module.exports = mongoose.model('User', User);
